import json
import os

mainDir = os.path.dirname(__file__)
userslistJson = os.path.join(mainDir, "users/users.json")
# userJson = os.path.join(mainDir, "users/{}.json".format(user.lower()))


def delUser(userslistJson, userJson, user):
    confirm = input("\nDo you really want to delete your user?\n(Y/N)\n--> ").lower()
    if confirm == "y":
        os.remove(userJson)
        with open(userslistJson, "r+") as data:
            userData = json.load(data)
        data = open(userslistJson, "w")
        userData["Users"].remove(user)
        del userData["Nicknames"][user]
        userData = str(userData).replace("'", "\"")
        data.write(userData)
        data.close()
        print("\nUser Deleted\n")
        return 1
    return 0


def addUser(userslistJson):
    userName = (input("\nEnter a name: "))
    data = open(userslistJson, "r+")
    userList = json.load(data)
    if userName in userList["Users"]:
        print("\nUser already registered as {}, {}".format(userName, userList["Nicknames"][userName]))
        return userName

    else:
        data.close()
        data = open(userslistJson, "w")
        userList["Users"].append(userName)
        nickName = input("Insert your nickname (eg: The Cook, The Explorer...): ")
        userList["Nicknames"][userName] = nickName
        data.write(json.dumps(userList))
        userJson = os.path.join(mainDir, "users/{}.json".format(userName.lower()))
        user_data = open(userJson, "w")
        user_data.write('''{{"basicInfo":{{"Name": \"{0}\","Nickname": \"{1}\"}},"finacialInfo":{{"balance":0, "Expenses":0, "Income":0}},"detailedIncome":{{"numItems":0}}, "detailedExpenses":{{"numItems":0}}}}'''.format(userName, nickName))
        # print("\nDone\n")
        return userName


def userCheck(userslistJson, user):
    data = open(userslistJson, "r+")
    userList = json.load(data)
    data.close()
    if user in userList["Users"]:
        return 1
    else:
        print("\nUser not registered. Please enter valid username or create a new one.\n")
        return 0
