from money_functions import *
from user_functions import *


def main():
    mainDir = os.path.dirname(__file__)
    userslistJson = os.path.join(mainDir, "users/users.json")
    user = input("What is your username?\n(Enter [new] to create a new one or [exit] to close the program.)\n--> ")
    if user == "exit":
        exit()
    elif user == "new":
        user = addUser(userslistJson)
    elif userCheck(userslistJson, user) == 0:
        return 1
    userJson = os.path.join(mainDir, "users/{}.json".format(user.lower()))
    operation = None
    commands = '''\n[1] to add income\n[2] to add expense\n[3] to see your account balance\n[4] to see all of your account details\n[5] to delete your user\n[exit] to quit the program\n'''
    print("\nLogged in as: {}".format(user))
    while operation != 0:
        operation = input("\nWhat would you like to do?\n(Type [help] for help)\n--> ")
        if operation == "help":
            print(commands)
        elif operation == "exit":
            print("Closing program...")
            return 0
        elif operation == "1":
            addIncome(userJson)
        elif operation == "2":
            addExpense(userJson)
        elif operation == "3":
            balance(userJson)
        elif operation == "4":
            fullDetails(userJson)
        elif operation == "5":
            if delUser(userslistJson, userJson, user) == 1:
                return 1
            else:
                continue

while True:
    x = main()
    if x == 0:
        break
    else:
        continue
