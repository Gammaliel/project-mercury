from datetime import datetime
import json
import os

mainDir = os.path.dirname(__file__)
userslistJson = os.path.join(mainDir, "users/users.json")


def addIncome(userJson):
    userFile = open(userJson, "r+")
    userData = json.load(userFile)
    userFile.close()

    name = input("\nHow would you like to call this income?\n--> ")
    while True:
        try:
            number = float(input("How much would you like to add to your expense?\n--> "))
            break
        except ValueError:
            print("\nInvalid number, please try again.\n")

    if name.isspace() or name == "":
        name = "Unnamed item {}".format(str(userData["detailedIncome"]["numItems"]))
        userData["detailedIncome"][name] = {}
        userData["detailedIncome"][name]["value"] = number
        userData["detailedIncome"]["numItems"] += 1
    else:
        userData["detailedIncome"][name] = {}
        userData["detailedIncome"][name]["value"] = number

    category = input("In whick category you'd like your expense to be in?\n--> ")
    if category.isspace() or category == "":
        category = "Uncategorized item."
        userData["detailedIncome"][name]["category"] = category

    userData["detailedIncome"][name]["category"] = category

    while True:
        dateIn = input("When was this done? (DD/MM/YYYY)\n--> ").strip(" ")
        if dateIn.isspace() or dateIn == "":
            validDate = "No date specified."
            break

        try:
            datetime.strptime(dateIn, '%d/%m/%Y')
            validDate = dateIn
            break
        except ValueError:
            print("\nInvalid date, please try again.\n")

    userData["detailedIncome"][name]["date"] = validDate

    userData["finacialInfo"]["Income"] = userData["finacialInfo"]["Income"] + number

    userFile = open(userJson, "r+")
    userFile.write(json.dumps(userData))
    userFile.close()
    print("Income added", "\n")


def addExpense(userJson):
    userFile = open(userJson, "r+")
    userData = json.load(userFile)
    userFile.close()

    name = input("How would you like to call this expense?\n--> ")
    number = float(input("How much would you like to add to your expense?\n--> "))
    if name.isspace() or name == "":
        name = "Unnamed item {}".format(str(userData["detailedExpenses"]["numItems"]))
        userData["detailedExpenses"][name] = {}
        userData["detailedExpenses"][name]["value"] = number
        userData["detailedExpenses"]["numItems"] += 1
    else:
        userData["detailedExpenses"][name] = {}
        userData["detailedExpenses"][name]["value"] = number

    category = input("In whick category you'd like your expense to be in?\n--> ")
    if category.isspace() or category == "":
        category = "Uncategorized item."
        userData["detailedExpenses"][name]["category"] = category

    userData["detailedExpenses"][name]["category"] = category

    while True:
        dateIn = input("When was this done? (DD/MM/YYYY)\n--> ").strip(" ")
        if dateIn.isspace() or dateIn == "":
            validDate = "No date specified."
            break
        try:
            datetime.strptime(dateIn, '%d/%m/%Y')
            validDate = dateIn
            break
        except ValueError:
            print("\nInvalid date, please try again.\n")

    userData["detailedExpenses"][name]["date"] = validDate

    userData["finacialInfo"]["Expenses"] = userData["finacialInfo"]["Expenses"] + number

    userFile = open(userJson, "r+")
    userFile.write(json.dumps(userData))
    userFile.close()
    print("Expense added", "\n")


def balance(userJson):
    userFile = open(userJson, "r+")
    userData = json.load(userFile)
    userFile.close()
    userData["finacialInfo"]["balance"] = userData["finacialInfo"]["Income"] - userData["finacialInfo"]["Expenses"]
    userFile = open(userJson, "r+")
    userFile.write(json.dumps(userData))

    print("Your account balance: ", userData["finacialInfo"]["Income"] - userData["finacialInfo"]["Expenses"], "\n")


def fullDetails(userJson):
    data = open(userJson, "r")
    userData = json.load(data)
    data.close()

    while True:
        choice = input("\nWhich information would like to see?\n--> ").lower()
        if choice == "1":
            for name, details in userData["detailedIncome"].items():
                if name == "numItems":
                    continue
                print("\nNAME: {} ".format(name))

                for item, value in details.items():
                    print("{}: {}".format(item.upper(), value))

        elif choice == "2":
            for name, details in userData["detailedExpenses"].items():
                if name == "numItems":
                    continue
                print("\nNAME: {} ".format(name))

                for item, value in details.items():
                    print("{}: {}".format(item.upper(), value))

        elif choice == "3":
            print("\nINCOME:")
            for name, details in userData["detailedIncome"].items():
                if name == "numItems":
                    continue
                print("\nNAME: {} ".format(name))

                for item, value in details.items():
                    print("{}: {}".format(item.upper(), value))

            print("\nEXPENSES:")
            for name, details in userData["detailedExpenses"].items():
                if name == "numItems":
                    continue
                print("\nNAME: {} ".format(name))

                for item, value in details.items():
                    print("{}: {}".format(item.upper(), value))

        elif choice == "return":
            break

        elif choice == "help":
            print("\nEnter:\n[1] for a list of your expense\n[2] for your expenses\n[3] for both\n[return] to go back")

        else:
            print("\nNot a valid command, enter [help] for a list of commands")
